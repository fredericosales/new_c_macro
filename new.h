/*
 *    GNU GENERAL PUBLIC LICENSE
 *     Version 3, 29 June 2007
 *
 * Frederico Sales
 * <frederico.sales@engenharia.ufjf.br>
 * 2017
 *
 *  new.h
 * 
 */

#ifndef NEW_H
#define NEW_H

// include
#include <stdlib.h>
#include <math.h> 
#include <string.h>
#include <ctype.h>
#include <stdbool.h>


// define

// new
#define new(type) ((type *) malloc(sizeof(type)))

// neo
#define neo(type, length) malloc(sizeof(type *) * length)

// delete
#define delete(x) free(x)

// swap XOR
#define swap(a, b) a ^= b ^= a ^= b

// Count elements of an char array
#define COUNT_ARRAY(X) sizeof(X) / sizeof(X[0])

// Count elements of an string (char *var) including spaces
#define COUNT_STRING(Y) strlen((const char *)Y)

// check var type
#define typeof(x, type) _Generic(x, type: true, default: false)

// struct


// var
typedef char *string;

// prototype
int *str_split(char *, const char *);
int *cycle(int, int, int, bool);

// functions
#include "new.c"    // not so pretty, however...

#endif
