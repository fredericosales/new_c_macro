/*
 *    GNU GENERAL PUBLIC LICENSE
 *     Version 3, 29 June 2007
 *
 * Frederico Sales
 * <frederico.sales@engenharia.ufjf.br>
 * 2017
 *
 *  new.c
 * 
 */

// include
#include "new.h"

// str split
int *str_split (char *str, const char *sep) {
  // strtok
  int *aux;gn
  int i = 0;
  char *token = NULL;
  char *aux0;

  aux = malloc (sizeof (int *) * 4);
  aux0 = malloc (sizeof (char *) * 15);

  token = strtok (strcpy (aux0, str), sep);

  while (token != NULL) {
      aux[i] = atoi (token);
      i++;
      token = strtok (NULL, sep);
    }

  return aux;
}


int *cycle(int period, int beg, int end, bool debug) {
    int jump;
    size_t i = 0;
    size_t j = 0;

    // jumpsjumps
    jump = 0;
    jump = (end - beg) / period;

    // vector
    // int jumps[jump];
    int *jumps = neo(int, jump+2);
    jumps[1] = beg;

    for (i = 2; i <= jump; i++) {
        beg += period;
        jumps[i] = beg;
    }

    if(jumps[i] != end) {
        jumps[i] = end;
    }
    jumps[0] = i+1;

    if(debug == true) {
        for (j = 0; j <= i; j++) {
            printf("jump: %ld: [%d]\n", j, jumps[j]);
        }
    }
    return jumps;
}
