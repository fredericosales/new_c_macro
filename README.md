# new() macro to C language
--------


### Warnings:
--------

	new_c_macro is free software under GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 
	without any warranty. Use it at your only risk.

	Use the force padawan, search in the source.

--------
### Installing c new macro
--------

#### Open a terminal (xterm, Terminal, xfc4-terminal):

#### Clone with HTTPS:
```shell
user@madmonkey[~/]$: git clone https://gitlab.com/fredericosales/new_c_macro.git
```

#### Clone with SSH:
```shell
user@madmonkey[~/]$: git clone git@gitlab.com:fredericosales/new_c_macro.git
```


#### System install, execute with superuser or sudo.

#### Superuser:
```shell
root@madmonkey[/root/new_c_macro/]#: bash install
```
or
#### sudo:
```shell
user@madmonkey[~/new_c_macro]$: sudo bash install
```

--------
### Using:
--------
#### To use new.h:
	Local:
	#include "path/to/new.h"

	System include:
	#include <new.h>

--------
### Features:
--------

	new:
	type *var = new(type);

	neo:
	type *var = neo(type, length);

	delete:
	delete(var);

	XOR swap:
	swap(var1, var2);

	Count array:
	COUNT_ARRAY(array);

	Count string:
	COUNT_STRING(string);

	Split string:
	str_split("string", "separator");

	cycle:
	cycle(jump, begin, end, boolean);

--------------
### Example:
--------------

#### main.c

```c

// include
#include <stdio.h>
#include <new.h>

// struct
typedef struct node{
	int data;
	struct node *next;
}NODE;

// main
int main(int argc, char **argv) {
	// new
	NODE *linked = new(NODE);
	// do your thing

	// neo
	char *bla = neo(char, 10);

	// delete
	delete(linked);
	delete(bla);

	// swap
	char *str0 = "some string";
	char *str1 = "another string";
	swap(str0, str1);

	int num0 = 10;
	int num1 = 20;
	swap(num0, num1);

	// And so ...

	// Count array
	char array[] = {"A", "B", "C","D"};
	printf("Elements of array[]:\t%ld\n", COUNT_ARRAY(array));

	// Count string
	string some = "here comes the pain";
 	printf("Chars:\t %ld\n", COUNT_STRING(some));

	// typeof
	int var;
	typeof(var, float);	// return false
	typeof(var, int); 	// return true

	// str_plit(char *, const char *)
	int *t = neo(int, 10);
	t = str_split("200.121.89.60", ".");
	for(int i = 0; i < 4; i++) {
		printf(" t[%d]: %d\t", i, t[i];)
	}
	printf("\n");
	delete(t);

	// cycle
	int *louc = neo(int, 100);
	louc = cycle(10, 1990, 2020, false);

	printf("ppk[");
	for (size_t k = 1; k < louc[0]; k++) {
		printf(" %d ", louc[k]);
	}
	printf("]\n");
	delete(louc);
	
	return 0;
}
```
